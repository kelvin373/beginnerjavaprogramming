/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Section1;

/**
 *
 * @author KELVIN
 */
public class Variables {
    public static void main(String[] args) {
        int a = 1000000000; // ini adalah variabel a dalam bentuk integer
        float b = 2.1f; // ini adalah variabel b dalam bentuk float
        double c = 3.14; // ini adalah variabel c dalam bentuk double
        char d = 'e'; // ini adalah variabel d dalam bentuk char
        boolean e = true; // ini adalah variabel e dalam bentuk boolean
        
        String f = "Algokelvin belajar java programming"; // ini adalah variabel f dalam bentuk String
        
        // Use println
        System.out.println("Ini adalah hasil tampilan dengan println");
        System.out.println(a);
        System.out.println(b);
        
        // Use print
        System.out.println("Ini adalah hasil tampilan dengan print");
        System.out.print(a);
        System.out.print(b);
        
        System.out.println("\n");
        
        //Menampilkan hasil nilai
        System.out.println("Ini adalah proses menampilkan variabel di atas");
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println(e);
        System.out.println(f);
        
        //Menampilkan hasil nilai
        System.out.println("\nTampilan ke 2");
        System.out.println("Algokelvin");
        System.out.println(a+"\n"+b+"\n"+c+"\n"+d+"\n"+e);
        System.out.println(f+" dengan senang");
        
        // Main operasi matematika
        System.out.println("\nOperasi matematika");
        int A = 1/3;
        float B = (float) 1/3;
        double C = (double) 1/3;
        System.out.println(A+"\n"+B+"\n"+C);
        
        System.out.println("\nOperasi Matematika 2");
        int X = 1+3;
        int Y = 1-3;
        int Z = 1*3;
        System.out.println(X+"\n"+Y+"\n"+Z);
        
        System.out.println("\nOperasi Matematika 3");
        int x = 5;
        x = x + 1; // --> x += 1 --> 2 operasi
        System.out.println(x); // hasilnya 6
        x += 1; // --> 1 operasi
        System.out.println(x); // hasilnya 7
    }
}
