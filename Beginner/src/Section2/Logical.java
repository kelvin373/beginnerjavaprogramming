
package Section2;

public class Logical {
    public static void main(String[] args) {
        int a = 3, b = 1;
        int c = -7, d = -9;
        
        System.out.println("Relational Operator :");
        System.out.println(a < b); // True
        System.out.println(a > b); // False
        System.out.println(c > b); // False
        System.out.println(d < c); // True
        
        int e = 5;
        System.out.println("Relational Operator 2 : ");
        System.out.println(e == b); // True
        System.out.println(e == c); // False
        System.out.println(e != b); // False
        System.out.println(e != d); // True
        
        System.out.println("About Decision : ");
        if (e == b) {
            System.out.println("Masuk A");
        }
        else {
            System.out.println("Masuk B");
        }
        
        int r = 10;
        System.out.println("About Decision 2 : ");
        float lingkaran = 100f;
        System.out.println(lingkaran);
        if (lingkaran <= 100) {
            System.out.println("Masuk C");
        }
        else {
            System.out.println("Masuk D");
        }
        
        char x = 'x', y = 'y';
        if (x != y) {
            System.out.println("Masuk E");
        }
        else {
            System.out.println("Masuk F");
        }
        
        String A = "aku";
        String B = "kamu";
        if (A.equals(B)) {
            System.out.println("Masuk G");
        }
        else {
            System.out.println("Masuk H");
        }
    }
}
